﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using IdentityModel.Extensions;
using IdentityServer3.AccessTokenValidation;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Owin;

[assembly: OwinStartup(typeof(WebApplication4.Startup))]

namespace WebApplication4
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
            {
                Authority = "http://www.testauth2.com/",
                RequiredScopes = new[] { "api1" },
                ClientId = "api1",
                DelayLoadMetadata = false,
                AuthenticationMode = AuthenticationMode.Active,
                AutomaticRefreshInterval = new TimeSpan(0, 10, 0),
                EnableValidationResultCache = false
            });

            var config = new HttpConfiguration();
            // Web API 配置和服务
            config.EnableCors(new EnableCorsAttribute("*", "*", "*")
            {
                SupportsCredentials = true
            });

            // Web API 路由
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Filters.Add(new AuthorizeAttribute());
            app.UseWebApi(config);
        }
    }
}