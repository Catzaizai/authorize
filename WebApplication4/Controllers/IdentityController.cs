﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Owin;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    [System.Web.Mvc.Route("identity")]
    [System.Web.Mvc.Authorize]
    public class IdentityController : ApiController
    {
        public async Task<IHttpActionResult> Get()
        {
            var client = new HttpClient();
            client.SetBearerToken(HttpContext.Current.Request.Headers["Authorization"].Replace("Bearer ", ""));

            var response = await client.GetAsync("http://www.testauth2.com/connect/userinfo");
            var user = await response.Content.ReadAsAsync<User>();
            return Json(user);
        }
    }
}
