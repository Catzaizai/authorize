﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication4.Models
{
    public class User
    {
        public string Username { get; set; }

        public string Nickname { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public DateTime LastLoginDate { get; set; }

        public string Sub { get; set; }
    }
}