﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Authorize.Core.Enums
{
    public enum ClientType
    {
        [Description("前端")]
        FrondEnd = 1,

        [Description("后端")]
        BackEnd
    }
}
