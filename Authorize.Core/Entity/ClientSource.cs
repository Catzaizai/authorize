﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Authorize.Core.Enums;

namespace Authorize.Core.Entity
{
    [Table("ClientSource")]
    public class ClientSourceEntity : BaseEntity<Guid>
    {
        public string ClientId { get; set; }

        public string ClientName { get; set; }

        public string ClientSecrets { get; set; }

        public ClientType Type { get; set; }

        public string RedirectUris { get; set; }

        [StringLength(500)]
        public string PostLogoutRedirectUris { get; set; }

        [StringLength(500)]
        public string AllowedCorsOrigins { get; set; }

        [StringLength(500)]
        public string AllowedScopes { get; set; }
    }
}
