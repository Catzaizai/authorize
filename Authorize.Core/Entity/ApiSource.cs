﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Authorize.Core.Entity
{
    [Table("ApiSource")]
    public class ApiSourceEntity : BaseEntity<Guid>
    {
        [Required]
        [StringLength(50)]
        public string ApiCode { get; set; }

        [StringLength(50)]
        public string ApiName { get; set; }
    }
}
