﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Authorize.Core.Entity
{
    [Table("Users")]
    public class UserEntity : BaseEntity<Guid>
    {
        /// <summary>
        /// oidc 标识 Id
        /// </summary>
        [StringLength(256)]
        public string SubId { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        [StringLength(50)]
        public string Username { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [StringLength(128)]
        public string Password { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [StringLength(50)]
        public string Nickname { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [StringLength(20)]
        public string Phone { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        /// <summary>
        /// 微信 开放id
        /// </summary>
        [StringLength(128)]
        public string WeiChatOpenId { get; set; }

        /// <summary>
        /// 终端 开放id
        /// </summary>
        [StringLength(128)]
        public string TerminalOpenId { get; set; }

        public DateTime LastLoginDate { get; set; }

        public int Sex { get; set; }

        public string AvartarUrl { get; set; }
    }
}
