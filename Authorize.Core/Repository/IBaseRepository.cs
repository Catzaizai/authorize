﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Authorize.Core.Entity;

namespace Authorize.Core.Repository
{
    public interface IBaseRepository<TEntity, in TKey> where TEntity : BaseEntity<TKey>
    {
        TEntity Add(TEntity entity);

        bool Delete(TKey key);

        TEntity Modify(TEntity entity);

        TEntity Get(TKey key);

        TEntity GetFirst(Expression<Func<TEntity, bool>> query);

        Task<TEntity> GetFirstAsync(Expression<Func<TEntity, bool>> query);

        List<TEntity> GetAll(Expression<Func<TEntity, bool>> query);

        List<TEntity> GetAll();

        Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> query);
    }
}
