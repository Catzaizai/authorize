﻿using System;
using System.Collections.Generic;
using System.Text;
using Authorize.Core.Entity;

namespace Authorize.Core.Repository
{
    public interface IClientSourceRepository : IBaseRepository<ClientSourceEntity, Guid>
    {
    }
}
