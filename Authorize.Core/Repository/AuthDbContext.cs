﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Authorize.Core.Configuration;
using Authorize.Core.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Authorize.Core.Repository
{
    public sealed class AuthDbContext : DbContext
    {
        private static string _connectString;

        public AuthDbContext()
        {
            var configuration = AppConfigurations.Get(Directory.GetCurrentDirectory());
            _connectString = configuration.GetConnectionString("Default");
            Database.EnsureCreated();
        }

        public DbSet<UserEntity> Users { get; set; }

        public DbSet<ClientSourceEntity> Clients { get; set; }

        public DbSet<ApiSourceEntity> ApiSources { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => 
            optionsBuilder.UseMySql(_connectString);
    }
}
