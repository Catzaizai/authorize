﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Authorize.Core.Cache;
using Authorize.Core.Extensions;
using Authorize.Core.Repository;
using Authorize.Core.Repository.Implement;
using Authorize.Core.Store;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.Extensions.Logging;

namespace Authorize.Core.IdentityServer
{
    public class CorsPolicyService : ICorsPolicyService
    {
        protected readonly IEnumerable<Client> Clients;

        public CorsPolicyService()
        {
            Clients = ConstDataCache.ClientSourceCache;
        }

        public Task<bool> IsOriginAllowedAsync(string origin)
        {
            var query = from client in Clients from url in client.AllowedCorsOrigins select url.GetOrigin();

            var result = query.Contains(origin, StringComparer.OrdinalIgnoreCase);

            return Task.FromResult(result);
        }
    }
}
