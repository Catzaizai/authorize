﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;

namespace Authorize.Core.Authentication.WeChat
{
    public static class WeChatCache
    {
        private static Dictionary<string, AuthenticationProperties> _propertieses;

        private static Dictionary<string, AuthenticationProperties> Propertieses => _propertieses ?? (_propertieses = new Dictionary<string, AuthenticationProperties>());

        public static AuthenticationProperties GetProperties(string key)
        {
            var properties = Propertieses[key];

            void WorkItem(object status)
            {
                Thread.Sleep(3000);
                Propertieses.Remove(key);
            }

            ThreadPool.QueueUserWorkItem(WorkItem);

            //Task.Run(() =>
            //{
            //    Thread.Sleep(3000);
            //    Propertieses.Remove(key);
            //});
            // Propertieses.Remove(key);
            return properties;
        }

        public static void SetProperties(string key, AuthenticationProperties properties)
        {
            Propertieses.Add(key, properties);
        }
    }
}
