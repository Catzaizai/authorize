﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authorize.Core.Authentication
{
    public static class CustomProfileDefault
    {
        public static string UserName = "username";

        public static string Phone = "phone";

        public static string Email = "email";

        public static string NickName = "nickname";

        public static string Id = "id";

        public static string LastLoginDate = "lastLoginDate";

        public static string OpenId = "openId";

        public static string DeviceId = "deviceId";

        public static string Sex = "sex";

        public static string AvartarUrl = "avartarUrl";

        public static string CreateDate = "createDate";
    }
}
