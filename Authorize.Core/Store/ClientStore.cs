﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Authorize.Core.Cache;
using Authorize.Core.Entity;
using Authorize.Core.Enums;
using Authorize.Core.Repository;
using Authorize.Core.Repository.Implement;
using IdentityServer4.Models;
using IdentityServer4.Stores;

namespace Authorize.Core.Store
{
    public class ClientStore : IClientStore
    {
        public Task<Client> FindClientByIdAsync(string clientId)
        {
            var client = ConstDataCache.ClientSourceCache.Find(x => x.ClientId == clientId);
            return Task.FromResult(client);
        }    
    }
}
