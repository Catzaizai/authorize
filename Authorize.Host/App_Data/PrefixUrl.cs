﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Authorize.Core.Configuration;
using Microsoft.Extensions.Configuration;

namespace Authorize.Host.App_Data
{
    public static class PrefixUrl
    {   
        public const string Prefix = "test/authorize/";

        public static string PublicOrigin { get; set; }
    }
}
