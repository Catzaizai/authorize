﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Authorize.Application.ApiSource;
using Authorize.Application.ApiSource.Model;
using Authorize.Host.App_Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Authorize.Host.Controllers
{
    public class ApiSourceController : Controller
    {
        private readonly IApiSourceService _service;

        public ApiSourceController()
        {
            _service = new ApiSourceService();
        }

        // GET: ApiSource
        public ActionResult Index()
        {
            var list = _service.GetList();
            return View(list);
        }

        // GET: ApiSource/Details/5
        public ActionResult Details(Guid id)
        {
            var model = _service.Get(id);
            return View(model);
        }

        // GET: ApiSource/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ApiSource/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ApiSourceInputModel model)
        {
            try
            {
                _service.Create(model);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ApiSource/Edit/5
        public ActionResult Edit(Guid id)
        {
            var model = _service.Get(id);
            return View(model);
        }

        // POST: ApiSource/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ApiSourceModel model)
        {
            try
            {
                _service.Modify(model);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ApiSource/Delete/5
        public ActionResult Delete(Guid id)
        {
            var model = _service.Get(id);
            return View(model);
        }

        // POST: ApiSource/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Guid id, IFormCollection collection)
        {
            try
            {
                _service.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}