﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Authorize.Application.ClientSource;
using Authorize.Application.ClientSource.Model;
using Authorize.Host.App_Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Authorize.Host.Controllers
{
    public class ClientSourceController : Controller
    {
        private readonly IClientSourceService _service;
        public ClientSourceController()
        {
            _service = new ClientSourceService();
        }
        // GET: ClientSource
        public ActionResult Index()
        {
            var modelList = _service.GetList();
            return View(modelList);
        }

        // GET: ClientSource/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ClientSource/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ClientSource/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClientSourceInputModel model)
        {
            try
            {
                _service.Create(model);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View(model);
            }
        }

        // GET: ClientSource/Edit/5
        public ActionResult Edit(Guid id)
        {
            var model = _service.Get(id);
            return View(model);
        }

        // POST: ClientSource/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ClientSourceModel model)
        {
            try
            {
                _service.Modify(model);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ClientSource/Delete/5
        public ActionResult Delete(Guid id)
        {
            var model = _service.Get(id);
            return View(model);
        }

        // POST: ClientSource/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Guid id, IFormCollection collection)
        {
            try
            {
                _service.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}