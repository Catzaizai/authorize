﻿using System;
using System.Collections.Generic;
using System.Text;
using Authorize.Application.ApiSource.Model;
using Authorize.Application.ClientSource.Model;
using Authorize.Core.Entity;
using AutoMapper;

namespace Authorize.Application
{
    public static class AutoMapper
    {
        public static void InitAutoMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ClientSourceEntity, ClientSourceModel>();
                cfg.CreateMap<ClientSourceModel, ClientSourceEntity>();

                cfg.CreateMap<ClientSourceEntity, ClientSourceInputModel>();
                cfg.CreateMap<ClientSourceInputModel, ClientSourceEntity>();

                cfg.CreateMap<ApiSourceEntity, ApiSourceModel>();
                cfg.CreateMap<ApiSourceModel, ApiSourceEntity>();

                cfg.CreateMap<ApiSourceEntity, ApiSourceInputModel>();
                cfg.CreateMap<ApiSourceInputModel, ApiSourceEntity>();
            });
        }
    }
}
