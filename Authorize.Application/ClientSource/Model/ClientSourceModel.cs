﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Authorize.Core.Enums;

namespace Authorize.Application.ClientSource.Model
{
    public class ClientSourceModel
    {
        [Key]
        public Guid Id { get; set; }

        [Display(Name = "ClientId")]
        public string ClientId { get; set; }

        [Display(Name = "ClientName")]
        public string ClientName { get; set; }

        [Display(Name = "ClientSecrets")]
        public string ClientSecrets { get; set; }

        [Display(Name = "客户端类型")]
        public ClientType Type { get; set; }

        [Display(Name = "跳转地址")]
        public string RedirectUris { get; set; }

        [Display(Name = "退出登录跳转")]
        public string PostLogoutRedirectUris { get; set; }

        [Display(Name = "前端跨域地址（前端地址）")]
        public string AllowedCorsOrigins { get; set; }

        [Display(Name = "允许访问范围")]
        public string AllowedScopes { get; set; }
    }
}
