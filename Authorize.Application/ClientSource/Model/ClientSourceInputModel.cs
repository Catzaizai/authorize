﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Authorize.Core.Enums;

namespace Authorize.Application.ClientSource.Model
{
    public class ClientSourceInputModel
    {
        [Required]
        [Display(Name = "ClientId")]
        public string ClientId { get; set; }

        [Required]
        [Display(Name = "ClientName")]
        public string ClientName { get; set; }

        [Display(Name = "ClientSecrets")]
        public string ClientSecrets { get; set; }

        [Required]
        [Display(Name = "客户端类型")]
        [EnumDataType(typeof(ClientType))]
        public ClientType Type { get; set; }

        [Required]
        [Display(Name = "跳转地址")]
        public string RedirectUris { get; set; }

        [Required]
        [Display(Name = "退出登录跳转")]
        [StringLength(500)]
        public string PostLogoutRedirectUris { get; set; }

        [Required]
        [Display(Name = "前端跨域地址（前端地址）")]
        [StringLength(500)]
        public string AllowedCorsOrigins { get; set; }

        [Required]
        [Display(Name = "允许访问范围")]
        [StringLength(500)]
        public string AllowedScopes { get; set; }
    }
}
