﻿using System;
using System.Collections.Generic;
using System.Text;
using Authorize.Application.ClientSource.Model;

namespace Authorize.Application.ClientSource
{
    public interface IClientSourceService
    {
        List<ClientSourceModel> GetList();

        ClientSourceModel Create(ClientSourceInputModel model);

        ClientSourceModel Get(Guid id);

        bool Modify(ClientSourceModel model);

        bool Delete(Guid id);
    }
}
