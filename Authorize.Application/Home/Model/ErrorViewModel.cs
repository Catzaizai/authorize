﻿using IdentityServer4.Models;

namespace Authorize.Application.Home.Model
{
    public class ErrorViewModel
    {
        public ErrorMessage Error { get; set; }
    }
}