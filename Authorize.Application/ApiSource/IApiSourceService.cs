﻿using System;
using System.Collections.Generic;
using System.Text;
using Authorize.Application.ApiSource.Model;

namespace Authorize.Application.ApiSource
{
    public interface IApiSourceService
    {
        List<ApiSourceModel> GetList();

        ApiSourceModel Get(Guid id);

        ApiSourceModel Create(ApiSourceInputModel model);

        bool Modify(ApiSourceModel model);

        bool Delete(Guid id);
    }
}
