﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Authorize.Application.ApiSource.Model
{
    public class ApiSourceInputModel
    {
        [Required]
        [StringLength(50)]
        [Display(Name = "api code")]
        public string ApiCode { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "备注名称")]
        public string ApiName { get; set; }
    }
}
