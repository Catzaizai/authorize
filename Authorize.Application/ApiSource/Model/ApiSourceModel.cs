﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Authorize.Application.ApiSource.Model
{
    public class ApiSourceModel
    {
        [Key]
        public Guid Id { get; set; }

        [Display(Name = "api code")]
        public string ApiCode { get; set; }

        [Display(Name = "备注名称")]
        public string ApiName { get; set; }
    }
}
