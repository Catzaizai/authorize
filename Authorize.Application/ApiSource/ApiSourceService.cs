﻿using System;
using System.Collections.Generic;
using System.Text;
using Authorize.Application.ApiSource.Model;
using Authorize.Core.Cache;
using Authorize.Core.Entity;
using Authorize.Core.Repository;
using Authorize.Core.Repository.Implement;
using AutoMapper;

namespace Authorize.Application.ApiSource
{
    public class ApiSourceService : IApiSourceService
    {
        private readonly IApiSourceRepository _repository;

        public ApiSourceService()
        {
            _repository = new ApiSourceRepository();
        }

        public List<ApiSourceModel> GetList()
        {
            var entities = _repository.GetAll();
            return Mapper.Map<List<ApiSourceModel>>(entities);
        }

        public ApiSourceModel Get(Guid id)
        {
            return Mapper.Map<ApiSourceModel>(_repository.GetFirst(x => x.Id == id));
        }

        public ApiSourceModel Create(ApiSourceInputModel model)
        {
            var entity = Mapper.Map<ApiSourceEntity>(model);
            entity.Id = Guid.NewGuid();
            entity.CreateDate = DateTime.Now;
            entity.UpdateDate = DateTime.Now;
            _repository.Add(entity);
            ConstDataCache.UpdateApiResource(entity);
            return Mapper.Map<ApiSourceModel>(entity);
        }

        public bool Modify(ApiSourceModel model)
        {
            var entity = _repository.GetFirst(x => x.Id == model.Id);
            entity.ApiCode = model.ApiCode;
            entity.ApiName = model.ApiName;
            entity.UpdateDate = DateTime.Now;
            _repository.Modify(entity);
            ConstDataCache.UpdateApiResource(entity);
            return true;
        }

        public bool Delete(Guid id)
        {
            var entity = _repository.Get(id);
            ConstDataCache.Remove(entity.ApiCode);
            return _repository.Delete(id);
        }
    }
}
