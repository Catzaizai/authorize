﻿using System.ComponentModel.DataAnnotations;

namespace Authorize.Application.Account.Model
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "密码是必填字段")]
        [StringLength(20)]
        [Display(Name = "密码")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "确认密码")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "两次密码必须相等")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "手机号是必填字段")]
        [StringLength(20)]
        [Display(Name = "电话号码")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [StringLength(100)]
        [Display(Name = "邮箱")]
        [EmailAddress]
        public string Email { get; set; }

        public bool HasError { get; set; }

        public string Message { get; set; }
    }
}
