﻿using System.ComponentModel.DataAnnotations;

namespace Authorize.Application.Account.Model
{
    public class LoginInputModel
    {
        [Required(ErrorMessage = "手机号不能为空")]
        [Display(Name = "手机号")]
        public string Username { get; set; }

        [Required(ErrorMessage = "密码不能为空")]
        [Display(Name = "密码")]
        public string Password { get; set; }

        [Display(Name = "记住登录")]
        public bool RememberLogin { get; set; }

        public string ReturnUrl { get; set; }
    }
}